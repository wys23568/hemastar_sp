
const service = require('/api/server.js');
App({
  globalData: {
    userInfo: null,
    scopeUser: true,  //true: 已授权； false：未授权
  },
  onLaunch: function () {
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              this.globalData.userInfo = res.userInfo
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回,所以此处加入 callback 以防止这种情况
            }
          })
        }else {
        }
      }
    })
  }
})