var config = require('../config.js');
const common = require('./common.js')
var MD5 = require('../utils/md5.js').hexMD5;    // 引入md5.js文件

function request(apiAddress, requestData,callback) {
  // wx.showLoading({
  //   title: '正在加载',
  // })
  var params = {
    // appId: "20111114",  //旧版appId
    // appKey: "b3826cf1sdf6ee6584ec2d3254ab731a",  //旧版appKey
    appId: "20111120",
    appKey: "df4ec2d3b3826cf1s254ab731a6ee658"
  }
  var requestObj = Object.assign(requestData,params)
  wx.request({
    url: config.host + apiAddress,
    method: 'POST',
    header: common.apiHeader,
    data: requestObj,
    success: res => {
      wx.hideLoading();
      common.result(res.data, callback);
    },
    fail: res => {
      wx.hideLoading();
    }
  })
}
function codeLogin(data, callback) {
  request(config.api.codeLogin, {
    // appId: "20111114",
    // appKey: "b3826cf1sdf6ee6584ec2d3254ab731a",
    appId: "20111120",
    appKey: "df4ec2d3b3826cf1s254ab731a6ee658",
    accountType: "student",
    code: wx.getStorageSync("code"),
    iv: data.iv,
    encryptedData: data.encryptedData
  }, callback)
}
function codePhone(data, callback) {
  request(config.api.codePhone, {
    ssoId: wx.getStorageSync("loginInfo").sso.id,
    oauthId: wx.getStorageSync("loginInfo").sso.oauthId,
    iv: data.iv,
    encryptedData: data.encryptedData
  }, callback)
}
// 获取用户信息
function getUserInfo(data, callback) {
  request(config.api.info, {
    loginToken: wx.getStorageSync("loginToken"),
    useFlag: data.useFlag,
    name: data.name,
    avatarUrl: data.avatarUrl
  }, callback)
}
// 更新用户信息
function updateInfo(data, callback) {
  request(config.api.update, data, callback)
}
// 意见反馈
function commentPush(data, callback) {
  request(config.api.commentPush, {
    loginToken: wx.getStorageSync("loginToken"),
    title: "小程序意见反馈",
    content: data.content,
    contact: data.contact,
  }, callback)
}
// 手机号授权绑定手机号
function bindPhone(data,callback) {
  request(config.api.bindPhone, {
    ssoId: wx.getStorageSync("loginInfo").sso.id,
    oauthId: wx.getStorageSync("loginInfo").sso.oauthId,
    iv: data.iv,
    encryptedData: data.encryptedData,
    code: wx.getStorageSync("code"),
  }, callback)
}
// 课程列表
function getCourseList(data, callback) {
  wx.showLoading({
    title: '正在加载',
  })
  wx.request({
    url: config.host + config.api.courseList,
    method: 'POST',
    header: common.apiHeader,
    data: {
      loginToken: wx.getStorageSync("loginToken"),
      pageNumber: data.pageNumber,
      pageSize: 10,
      source: 1,
      appId: "20111120",
      appKey: "df4ec2d3b3826cf1s254ab731a6ee658"
    },
    success: res => {
      wx.hideLoading();
      common.result(res.data, callback);
    },
    fail: res => {
      wx.hideLoading();
    }
  })
}
// 课程详情
function getCourseDetail(data, callback) {
  request(config.api.courseDetail, {
    loginToken: wx.getStorageSync("loginToken"),
    courseId: data.courseId,
    teacherCourseId: data.teacherCourseId,
  }, callback)
}
// 获取解绑手机号验证码
function unbindTel(callback) {
  request(config.api.unbindTel, {
    loginToken: wx.getStorageSync("loginToken"),
  }, callback)
}
// 确认解绑手机号码
function unbindTelCode(data, callback) {
  request(config.api.unbindTelCode, {
    loginToken: wx.getStorageSync("loginToken"),
    verifyCode: data.verifyCode,
  }, callback)
}
// 获取绑定手机号验证码
function bindTelCode(data, callback) {
  wx.request({
    url: config.host + config.api.bindTelCode,
    method: 'POST',
    header: common.apiHeader,
    data: {
      // appId: "20111114",
      // appKey: "b3826cf1sdf6ee6584ec2d3254ab731a",
      appId: "20111120",
      appKey: "df4ec2d3b3826cf1s254ab731a6ee658",
      loginToken: wx.getStorageSync("loginToken"),
      verifyPhone: data.verifyPhone,
    },
    success: res => {
      wx.hideLoading();
      if (res.data.code == 101) {
        wx.showModal({
          title: '温馨提示',
          content: res.data.error,
          showCancel: false,
          confirmText: '我知道了',
          confirmColor: '#02BB00'
        })
        return false;
      }
      common.result(res.data, callback);
    },
    fail: res => {
      wx.hideLoading();
    }
  })
}
// 确认绑定手机号码
function bindTel(data, callback) {
  request(config.api.bindTel, {
    loginToken: wx.getStorageSync("loginToken"),
    verifyPhone: data.verifyPhone,
    verifyCode: data.verifyCode,
  }, callback)
}
// 确认绑定手机号码
function getCoursePackage(callback) {
  request(config.api.coursePackage, {
    loginToken: wx.getStorageSync("loginToken"),
  }, callback)
}
function createOrder(data, callback) {
  console.log(data)
  wx.request({
    url: config.host + config.api.createOrder,
    method: 'POST',
    header: common.apiHeader,
    data: {
      loginToken: wx.getStorageSync("loginToken"),
      payChannel: 1,
      payType: 'weixin-jsapi',
      courseId: data.courseId,
    // appId: "20111114",
    // appKey: "b3826cf1sdf6ee6584ec2d3254ab731a",
      appId: "20111120",
      appKey: "df4ec2d3b3826cf1s254ab731a6ee658",
    },
    success: res => {
      if(res.data.code == 100) {
        wx.showToast({
          icon: 'none',
          title: res.data.error,
        })
      } else if (res.data.code == 200) {
        wx.requestPayment({
          timeStamp: res.data.data.payResult.timestamp,
          nonceStr: res.data.data.payResult.noncestr,
          package: 'prepay_id=' + res.data.data.payResult.prepayid,
          signType: 'MD5',
          paySign: res.data.data.payResult.paySign,
          success(payres) {
            var resAll = Object.assign(payres, res.data.data)
            callback(resAll)
          },
          fail(payres) {
            var resAll = Object.assign(payres, res.data.data)
            callback(resAll)
          }
        })
      } else {
        wx.showToast({
          icon: 'none',
          title: res.error,
        })
      }
    },
    fail: res => {
      wx.hideLoading();
    }
  });
}
// 订单列表
function orderList(data, callback) {
  request(config.api.orderList, {
    loginToken: wx.getStorageSync("loginToken"),
    pageNumber: data.pageNumber,
    pageSize: 10,
  }, callback)
}
// 订单详情
function orderDetail(data, callback) {
  request(config.api.orderDetail, {
    loginToken: wx.getStorageSync("loginToken"),
    orderNo: data.orderNo,
  }, callback)
}
// 获取二维码
function courseQrCode(data, callback) {
  request(config.api.courseQrCode, {
    loginToken: wx.getStorageSync("loginToken"),
    courseId: data.courseId,
  }, callback)
}

module.exports = {
  codeLogin,
  codePhone,
  getUserInfo,
  updateInfo,
  commentPush,
  bindPhone,
  getCourseList,
  unbindTel,
  unbindTelCode,
  bindTelCode,
  bindTel,
  getCoursePackage,
  getCourseDetail,
  createOrder,
  orderList,
  orderDetail,
  courseQrCode,

}