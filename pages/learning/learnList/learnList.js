const service = require("../../../api/server.js");
import { onGotUserInfo } from "../../../utils/util.js";
const app = getApp();
Page({
  data: {
    courseList: [],
    showCon: false,
    loginstate: "checking",
    showLoginModel: false,
  },
  onShow: function (options) {
    this.setData({
      loginstate: app.globalData.loginstate
    })
    if (!wx.getStorageSync("loginToken")) {
      wx.hideLoading();
      return false;
    }
    this.fetchCourseList();
  },
  fetchCourseList: function () {
    // 课程包列表
    service.getCourseList({ pageNumber: 0 }, res => {
      wx.hideLoading();
      this.setData({
        courseList: res.data.courseInfoList,
        showCon: true,
      })
    })  
  },
  onLoad: function (options) {
  },
  gotoDetail: function (e) {
    var data = e.currentTarget.dataset.data;
    console.log(data)
    wx.navigateTo({
      url: `../learning/learning?courseId=${data.id}&teacherCourseId=${data.teacherCourseId}`,
    })
  },
  switchTab: function () {
    wx.switchTab({
      url: '../../buyCourse/buyCourse',
    })
  },
  alertTip: function (e) {
    wx.showModal({
      title: '提示',
      content: '此课程尚未开课哦～',
      showCancel: false,
    })
  },
  onGotUserInfo: function (res) {
    this.setData({
      showLoginModel: false,
    })
    const result = onGotUserInfo(res);
    if(result) {
      app.globalData.loginstate = "hasLogin";
      this.setData({
        loginstate: "hasLogin"
      })
      var this_ = this;
      setTimeout(res => {
        this_.fetchCourseList();
      },1000)
    }
  },
  alertLogin: function () {
    this.setData({
      showLoginModel: !this.data.showLoginModel,
    })
  },
})