const service = require("../../../api/server.js");
const app = getApp();
Page({
  data: {
    courseId: '',
    courseInfo: {},
    showCon: true,
    nextUnlockIndex: '',
  },
  onShow: function (options) {
    if (!wx.getStorageSync("loginToken")) {
      this.setData({
        showCon: true,
        loginTokenNull: true,
      })
      return false;
    }
  },
  // 课程包详情
  requestDetail: function (params) {
    service.getCourseDetail(params, res => {
      this.setData({
        courseInfo: res.data
      })
      var that = this;
      if (res.data.courseLessonInfoList.find(function (val, index) {
        that.setData({
          nextUnlockIndex: index
        })
        return val.lockFlag == 1
      }) != 'undefined') {
      }
    })
  },
  onLoad: function (options) {
    console.log(options)
    // 课程包列表
    this.setData({
      courseId: options.courseId,
    })
    var params = {
      courseId: options.courseId,
      teacherCourseId: options.teacherCourseId
    }
    this.requestDetail(params); 
  },
  switchTab: function () {
    wx.switchTab({
      url: '../buyCourse/buyCourse',
    })
  },
  // 查看讲义
  checkBook: function (e) {
    wx.showLoading({
      title: '正在加载',
    })
    wx.downloadFile({
      url: e.currentTarget.dataset.book,
      success: function (res) {
        wx.showLoading({
          title: '正在打开',
        })
        var Path = res.tempFilePath //返回的文件临时地址，用于后面打开本地预览所用
        wx.openDocument({
          filePath: Path,
          success: function (res) {
            // console.log(res)
            wx.hideLoading()
          },
          fail: function (res) {
            // console.log(res)
            wx.showToast({
              icon: 'none',
              title: res.errMsg,
            })
            wx.hideLoading();
          }
        })
      },
      fail: function (res) {
        wx.hideLoading();
        wx.showToast({
          icon: 'none',
          title: res.errMsg,
        })
      }
    })
  },
  toCode: function () {
    wx.navigateTo({
      url: `../../code/code?courseId=${this.data.courseId}`,
    })
  },
  onShareAppMessage: function () {
    return {
      title: '属于孩子的编程课，让创造多一种形式',
      imageUrl: '../../../img/shareCon.png',
      path: '/pages/index/index',
      success: function () {
        wx.showToast({
          title: '分享成功',
        })
      }
    }
  }
})