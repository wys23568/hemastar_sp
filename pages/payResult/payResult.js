
const app = getApp()
const service = require("../../api/server.js");
Page({
  data: {
    payResult: '',
    courseId: '',
  },
  onLoad: function (options) {
    this.setData({
      payResult: options.status,
      courseId: options.courseId,
      orderNo: options.orderNo,
    })
    service.orderDetail({ orderNo: options.orderNo}, res => {
      // console.log("走起",res)
    })
  },
  // 返回购买首页
  toHome: function () {
    // console.log("走去")
    wx.switchTab({
      url: '../buyCourse/buyCourse',
    })
  },
  // 重新支付
  rePay: function () {
    var courseId = this.data.courseId;
    service.createOrder({ courseId }, res => {
      // console.log("结果", res)
    })
  },
  // 添加班主任老师
  toCode: function () {
    wx.navigateTo({
      url: `../code/code?courseId=${this.data.courseId}`,
    })
  },
  // 查看已购课程
  checkCourse: function () {
    wx.switchTab({
      url: '../learning/learnList/learnList',
    })
  }
})