const service = require("../../../api/server.js");
Page({
  data: {
    detailInfo: {},
  },
  onLoad: function (options) {
    service.orderDetail({ orderNo: options.orderno }, res => {
      this.setData({
        detailInfo: res.data,
      })
    })
  },
})